Purpose
=======
A place for me to keep track of projects I've created or contributed to. So basically
a portfolio of sorts


Credits
=======
The following people/sites are to be thanked
1. <a href="http://templated.co">TEMPLATED</a>
2. <a href="http://icons8.com">icons8</a>
3. <a href="http://github.com">Github</a>